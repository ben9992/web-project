﻿function sleep(milliseconds) {
    var start = new Date().getTime();
    for (var i = 0; i < 1e7; i++) {
        if ((new Date().getTime() - start) > milliseconds) {
            break;
        }
    }
}


let email = localStorage.getItem("email");
let password = localStorage.getItem("password");
let isAdmin = localStorage.getItem("isAdmin");
let Islogout = false;
if ((isAdmin == "False" || isAdmin == "false")) {
    alert("You're not admin user");
    window.location = "/Client/UserPage.html";
}
else if (email == null || password == null)
    window.location = "/index.html";


function openPage(pageName, elmnt) {
    // Hide all elements with class="tabcontent" by default */
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    // Remove the background color of all tablinks/buttons
    tablinks = document.getElementsByClassName("tablink");
    

    // Show the specific tab content
    document.getElementById(pageName).style.display = "block";


}

let ProductsData
window.onload = function () {
    showUsersTable();

    document.getElementById("HomePage").click();
    var table =
        '<table id="Maintable">' +
        ' <tr >' +
        '<th class="ui-helper-center">Picture</th>' +
        '<th class="ui-helper-center">Item</th>' +
        ' <th class="ui-helper-center">Catalog Number</th>' +
        '  <th class="ui-helper-center">Amount</th>' +
        ' <th class="ui-helper-center">Cost</th>' +
        '<th class="ui-helper-center">Price</th>' +
        '<th class="ui-helper-center">Category</th>' +
        '<th class="ui-helper-center">Is Available</th>' +


        '</table>';

    $(table).appendTo($("#table-Menu"));


    $.ajax({
        dataType: "json",			                         
        url: "/api/Products/All",			               
        contentType: "application/json; charset=utf-8",	   
        type: "GET",				                         
        			                 
        success: function (data) {                       
            
            ProductsData = data;
            for (i in data) {
                let $Maintr = $("<tr>");
                let isClicked = false;
                var pictd = $('<td>');
                if (data[i].img_url != null)
                    $('<img>').attr({ "class": "smallimg", "src": "/Client/Images/" + data[i].img_url }).appendTo(pictd);
                pictd.appendTo($Maintr);
                let name = $('<td>').html(data[i].Name).appendTo($Maintr);
                $('<td>').html(data[i].Catalog_number).appendTo($Maintr);
                let amount = $('<td id="td' + i + '">').html(data[i].Amount).appendTo($Maintr);
                $('<td>').html(data[i].Cost).appendTo($Maintr);
                $('<td>').html(data[i].Price).appendTo($Maintr);
                $('<td>').html(data[i].Category).appendTo($Maintr);
                $('<td>').html(data[i].isAvailable+"").appendTo($Maintr);
                let $temp = $('<td>');
                $temp.appendTo($Maintr);
                $($Maintr).appendTo("#Maintable");
            }
        }
    });

    $.ajax({
        dataType: "json",			                         
        url: "/api/Categories",			               
        contentType: "application/json; charset=utf-8",	   
        type: "GET",				                         
        			                 
        success: function (data) {                       
            for (i in data) {

                var Option = $('<option>').html(data[i].category_Name).appendTo("#category_Name");

            }
        }
    });
};



function logout() {
    localStorage.clear();
    location.reload();


}
function userPage() {
    window.location = "/Client/UserPage.html";
}
function ValidateNewItem(s)
{
    if (s.name == "" || s.price == "" || s.cost == "" || s.catalog_number == "" || s.amount == "" || s.img_url == "") {
        alert("One or more fields are empty!");
        return false
    }
    else if (isNaN(s.name) == false || isNaN(s.price) == true || isNaN(s.catalog_number) == true || isNaN(s.cost) == true || isNaN(s.amount) == true) {
        alert("All fields must be Numberable except Item Name and Img URL!");
        return false
    }
    for (i in ProductsData) {
        if (ProductsData[i].Catalog_number.replace(/\s/g, '') == s.catalog_number) {
            alert("Catalog Number Is Already exists!");
            return false 
        }
    }
    return true
}

function SubmitNewItem() {
    let s = {
        name: $("#Item-name").val(),
        price: $("#Price").val(),
        cost: $("#Cost").val(),
        catalog_number: $("#catalog_number").val(),
        amount: $("#amount").val(),
        img_url: $("#img_url_text").val()

    };
    if (ValidateNewItem(s) == false)
        return;
    
    $.ajax({
        dataType: "json",
        url: "/api/Products/" + $("#category_Name").val() + "/",
        contentType: "application/json; charset=utf-8",
        type: "POST",
        data: JSON.stringify(s),
        success: function (data) {
            SaveFile(s.catalog_number)
        }

    });
}
function SaveFile(id)
{
    var fd = new FormData();
    fd.append("file", $("#upfile")[0].files[0]);
    fd.append("id",id )

    $.ajax(
        {
            url: '/api/Products',
            data: fd,
            contentType: false,
            processData: false,
            type: 'PUT',
            success: function (data) {
               
                alert("Item Created!");
                location.reload();
            }
            

            });
}
function SubmitAccess() {
    $.ajax({

        dataType: "json",			                         
        url: "/api/Users/" + $("#ID").val() + "/" + $("#User-name").val() + "/" + $("#Email").val() + "/",			               
        contentType: "application/json; charset=utf-8",	   
        type: "POST",				                         
        			                 
        success: function (data) {                       
            alert("Permission granted for " + $("#User-name").val());
            location.reload();
        },
        error: function (vdata)
        { alert($("#User-name").val() + vdata.responseText.replace('"', ' ')) }
    });
}
function getFile() {
    document.getElementById("upfile").click();
}
function sub(obj) {
    var file = obj.value;
    var fileName = file.split("\\");
    document.getElementById("img_url_text").value = fileName[fileName.length - 1];


}
function showUsersTable() {
    var table = '<div id="table-users" class="table-users">' +
        '<div class="header">Users</div>' +
        '</div>'

    $(table).appendTo($("#container"));
    var table1 =
        '<table id="users" >' +
        ' <tr>' +
        '<th class="ui-helper-center">Name</th>' +
        '<th class="ui-helper-center">Email</th>' +
        '<th class="ui-helper-center">ID</th>' +
        '<th class="ui-helper-center">Password</th>' +
        '<th class="ui-helper-center">Is Admin</th>' +
        '</tr>' +
        '</table>';
    $(table1).appendTo($("#table-users"));

    $.ajax({
        dataType: "json",			                         
        url: "/api/Users",			               
        contentType: "application/json; charset=utf-8",	   
        type: "GET",				                         
        			                 
        success: function (data) {                       
            
            for (i in data) {
                let $Maintr = $("<tr>");
                let name = $('<td>').html(data[i].name).appendTo($Maintr);
                $('<td>').html(data[i].email).appendTo($Maintr);
                $('<td>').html(data[i].id).appendTo($Maintr);
                $('<td>').html(data[i].password).appendTo($Maintr);
                $('<td class="ui-helper-center">').html(data[i].isAdmin.toString()).appendTo($Maintr);


                $($Maintr).appendTo("#users");


            }
        }
    })
}







