﻿function sleep(milliseconds) {
    var start = new Date().getTime();
    for (var i = 0; i < 1e7; i++) {
        if ((new Date().getTime() - start) > milliseconds) {
            break;
        }
    }
}
function sortTable(n, tableName) {
    var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
    table = document.getElementById(tableName);
    switching = true;
    // Set the sorting direction to ascending:
    dir = "asc";
    /* Make a loop that will continue until
    no switching has been done: */
    while (switching) {
        // Start by saying: no switching is done:
        switching = false;
        rows = table.rows;
        /* Loop through all table rows (except the
        first, which contains table headers): */
        for (i = 1; i < (rows.length - 1); i++) {
            // Start by saying there should be no switching:
            shouldSwitch = false;
            /* Get the two elements you want to compare,
            one from current row and one from the next: */
            x = rows[i].getElementsByTagName("TD")[n];
            y = rows[i + 1].getElementsByTagName("TD")[n];
            /* Check if the two rows should switch place,
            based on the direction, asc or desc: */
            if (dir == "asc") {
                if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                    // If so, mark as a switch and break the loop:
                    shouldSwitch = true;
                    break;
                }
            } else if (dir == "desc") {
                if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                    // If so, mark as a switch and break the loop:
                    shouldSwitch = true;
                    break;
                }
            }
        }
        if (shouldSwitch) {
            /* If a switch has been marked, make the switch
            and mark that a switch has been done: */
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            switching = true;
            // Each time a switch is done, increase this count by 1:
            switchcount++;
        } else {
            /* If no switching has been done AND the direction is "asc",
            set the direction to "desc" and run the while loop again. */
            if (switchcount == 0 && dir == "asc") {
                dir = "desc";
                switching = true;
            }
        }
    }
}
let isAdmin = localStorage.getItem("isAdmin");
let email = localStorage.getItem("email");
let password = localStorage.getItem("password");
if (email == null || password == null) {
    window.location = "/index.html";
}
function openPage(pageName, elmnt) {
    // Hide all elements with class="tabcontent" by default */
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    // Remove the background color of all tablinks/buttons
    tablinks = document.getElementsByClassName("tablink");
    for (i = 0; i < tablinks.length; i++) {
        //tablinks[i].style.backgroundColor = "";
    }

    // Show the specific tab content
    document.getElementById(pageName).style.display = "block";

    if (pageName == "MyCart") {

        $(".EmptyCart").animate({ left: '100px' }, "fast");
        $(".EmptyCart").animate({ fontSize: '5em' }, "slow");
    }
    else {
        $(".EmptyCart").animate({ right: '100px' }, "fast");
        $(".EmptyCart").animate({ fontSize: '1em' }, "fast");
    }
    if (pageName == "Purchase_History") {

        $(".EmptyHistory").animate({ left: '100px' }, "fast");
        $(".EmptyHistory").animate({ fontSize: '5em' }, "slow");
    }
    else {
        $(".EmptyHistory").animate({ right: '100px' }, "fast");
        $(".EmptyHistory").animate({ fontSize: '1em' }, "fast");
    }
}


function loadPurchaseHistory() {
    $.ajax({
        dataType: "json",
        url: "/api/Purchases/" + email + "/" + password,
        contentType: "application/json; charset=utf-8",
        type: "GET",
        			                 
        success: function (data) {
            HistoryData = data.length;
            if (data.length == 0) {
                $("#table-Purchase").attr("class", "table-users hide")
                $("#DeletePurchase").attr("class", "btn btn-primary Mybtn hide")
                $(".EmptyHistory").attr("class", "EmptyHistory show")
            }
            for (let i = 0; i < data.length; i++) {
                for (let j = 0; j < ProductsData.length; j++) {
                    if (data[i].number == ProductsData[j].Catalog_number) {
                        let $Maintr = $("<tr>");
                        let name = $('<td>').html(ProductsData[j].Name).appendTo($Maintr);
                        let amount = $('<td id="td' + i + '">').html(data[i].amount).appendTo($Maintr);
                        var splited = data[i].purchase_date.replace('T', ' ').split('.');
                        $('<td style="text-align:center" >').html(splited[0]).appendTo($Maintr);
                        $($Maintr).appendTo("#Purchasetable");
                        break;
                    }
                }
            }
        }
    });

        


    $(document).ajaxSend(function (event, jqXHR, settings) {

        //$("#loader").css("display", "block");
        //$("#loader").fadeIn();

    });

    var ajaxSendCount = 0;
    $(document).ajaxComplete(function (event, jqXHR, settings) {

        if (HistoryData > 0 && ajaxSendCount == 1) {
            $("#table-Purchase").attr("class", "table-users show")
            $("#DeletePurchase").attr("class", "btn btn-primary Mybtn show")
            $(".EmptyHistory").attr("class", "hide")
        }

        if (ajaxSendCount == 1)
            $("#loader").fadeOut();

        ajaxSendCount++;

    });

}
let HistoryData = 0;
let sum = 0;
let ProductsData
window.onload = function () {

    if (isAdmin == "False" || isAdmin == "false")
        $("#Adminbtn").attr("class", "btn btn-primary Mybtn hide");

    document.getElementById("HomePage").click();
    var table =
        '<table id="Maintable">' +
        ' <tr >' +
        '<th class="ui-helper-center">Picture</th>' +
        '<th onclick="sortTable(0,Maintable.id)" class="ui-helper-center">Item <i class="fas fa-sort"></i></th>' +
        '<th onclick="sortTable(0,Maintable.id)" class="ui-helper-center">Catalog Number <i class="fas fa-sort"></i></th>' +
        '  <th onclick="sortTable(0,Maintable.id)" class="ui-helper-center">Amount <i class="fas fa-sort"></i></th>' +
        '<th onclick="sortTable(0,Maintable.id)" class="ui-helper-center">Price <i class="fas fa-sort"></i></th>' +
        '<th onclick="sortTable(0,Maintable.id)" class="ui-helper-center">Category <i class="fas fa-sort"></i></th>' +
        '<th class="ui-helper-center">Buy Me</th>' +
        '</tr>' +

        '</table>';

    $(table).appendTo($("#table-Menu"));

    
    $.ajax({
        dataType: "json",			                         
        url: "/api/Products",			               
        contentType: "application/json; charset=utf-8",	   
        type: "GET",				                         
        			                 
        success: function (data) {                       
            
            ProductsData = data;
            for (i in data) {
                let $Maintr = $("<tr>");
                let isClicked = false;
                var pictd = $('<td>');
                if (data[i].img_url != null)
                    $('<img>').attr({ "class": "smallimg", "src": "/Client/Images/" + data[i].img_url }).appendTo(pictd);
                pictd.appendTo($Maintr);
                let name = $('<td>').html(data[i].Name).appendTo($Maintr);
                $('<td>').html(data[i].Catalog_number).appendTo($Maintr);
                let amount = $('<td id="td' + i + '">').html(data[i].Amount).appendTo($Maintr);
                $('<td>').html(data[i].Price).appendTo($Maintr);
                $('<td>').html(data[i].Category).appendTo($Maintr);
                let $temp = $('<td>');

                var btn = $('<button >', {
                    //type: "button",
                    class: "btn",
                    click: function () {
                        for (var i = 0; i < 2; i++)
                            $(this).toggle(function () {
                                $(".inner").animate({ top: '-=10px' }, 500);
                            }, function () {
                                $(".inner").animate({ top: '+=10px' }, 500);
                            });
                        buyItem(name, amount);
                    }
                })
                btn.html('<i class="fas fa-cart-plus"></i>');
                btn.appendTo($temp);
                $temp.appendTo($Maintr);


                $($Maintr).appendTo("#Maintable");



            }
        },
        error: function () {

        }

    });



    loadPurchaseHistory();
};
function buyItem(th, amount) {
    if (amount.html() != 0) {
        amount.html(amount.html() - "1");
        addToCart(th, amount);
        $("#shoppingCount").html("   " + sum)
        $("#ClearCart").attr("class", "btn btn-primary Mybtn show");
        $("#BuyItems").attr("class", "btn btn-primary Mybtn show");
        $(".EmptyCart").attr("class", "hide");

    }
    else
        alert("This Item is Sold Out!");
}
function addToCart(th, amount) {
    if (sum == 0) {
        var cart = '<div id="table-cart" class="table-users">' +
            '<div class="header">Cart</div>' +
            '</div>'

        $(cart).appendTo($("#container"));
        var table1 =
            '<table id="Cart" >' +
            //' <tr>' +
            '<th class="ui-helper-center">Item</th>' +
            '<th class="ui-helper-center">Quantity</th>' +
            //'</tr>' +
            '</table>';
        $(table1).appendTo($("#table-cart"));


    }

    for (var i = 0; i < 2; i++)
        $('#shoppingCount').toggle(function () {
            $(".inner").animate({ top: '-=10px' }, 500);
        }, function () {
            $(".inner").animate({ top: '+=10px' }, 500);
        });
    


    var found = false;
    var table = document.getElementById("Cart");
    for (var i = 0, row; row = table.rows[i]; i++) {

        for (var j = 0, col; col = row.cells[j]; j++) {
            if (col.innerText == th.html()) {
                col = row.cells[j + 1];
                col.innerText = parseInt(col.innerText) + 1;
                found = true;
            }

        }
    }
    sum++;
    if (found == true)
        return;
    let $Maintr = $("<tr>");

    $('<td class="ui-helper-center">').html(th.html()).appendTo($Maintr);
    $('<td style="text-align:center">').html("1").appendTo($Maintr);

    $($Maintr).appendTo("#Cart");
}
function clearCart() {
    var ans = prompt("Please Write 'Yes' To Clear your cart");
    if (ans != "Yes" && ans != "yes")
        return
    sleep(700);
    location.reload();
}
function updateCart() {
    var table = document.getElementById("Cart");
    var purchase = [];
    for (var i = 1, row; row = table.rows[i]; i++) {

        for (var j = 0, col; col = row.cells[j]; j++) {

            var name = col.innerText;
            col = row.cells[j + 1];
            var amount = col.innerText;
            j++
            var person = { number: name, amount: amount };
            purchase.push(person)
            $.ajax({
                dataType: "json",			                         
                url: "/api/Products/" + parseInt(amount),			               
                contentType: "application/json; charset=utf-8",	   
                type: "PUT",				                         		
                data: JSON.stringify(name),			                 
                success: function (data) {                       
                    console.log("updating DB success.")
                },
                //error: inErrorCase

            });


        }
        
    }
    AddBuyRecord(purchase)
    setTimeout(function () {
        //do what you need here
    }, 6000);
    alert("Your cart is now empty, thank you for purchasing our products")
    sleep(700);
    location.reload();
}
function DeleteHistory() {

   
    var ans = prompt("Please Write 'Yes' To Delete your history");
    if (ans != "Yes" && ans != "yes")
        return

    $.ajax({

        dataType: "json",			                         
        url: "/api/Purchases/" + email + "/" + password,			               
        contentType: "application/json; charset=utf-8",	   
        type: "POST",				                         
        //data: JSON.stringify(p),			                 
        success: function (data) {                       
            console.log("Saving records success.")

        },
        // error: inErrorCase

    });

    sleep(1000);
    location.reload();

}
function AddBuyRecord(purchase) {
    
    $.ajax({

        dataType: "json",			                         
        url: "/api/Products/" + email + "/" + password, 			               
        contentType: "application/json; charset=utf-8",	   
        type: "POST",				                         
        data: JSON.stringify(purchase),			                 
        success: function (data) {                       
            console.log("Saving records success.")

        },
        // error: inErrorCase

    });
}
function logout() {
    location.reload();
    localStorage.clear();
}
function AdminPage() {

    window.location = "/Client/AdminPage.html";
}

