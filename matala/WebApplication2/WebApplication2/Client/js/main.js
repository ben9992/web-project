$('.message a').click(function(){
   $('form').animate({height: "toggle", opacity: "toggle"}, "slow");
});



function Login() {
    $("#Error").empty();

    if (!validateEmail($("#email").val())) {
        alert("Invalid Email Address format");
        return;
    }

    $.ajax({

        url: "/api/Users/" + ($("#email").val() + "/" + $("#password").val()),
        contentType: "application/json; charset=utf-8",
        type: "GET",
        success: function (data) {
            localStorage.setItem("email", $("#email").val());
            localStorage.setItem("password", $("#password").val());
            localStorage.setItem("isAdmin", data);
            if (data == false)
                window.location.replace("/Client/UserPage.html");
            else
                window.location.replace("/Client/AdminPage.html");
        },
        error: inErrorCase

    });
    
}


var input = document.getElementById("password");
var input2 = document.getElementById("email");
input2.addEventListener("keyup", function (event) {

    if (event.keyCode === 13) {

        document.getElementById("submitBtn").click();
    }

});
input.addEventListener("keyup", function (event) {
    
    if (event.keyCode === 13) {
       
        document.getElementById("submitBtn").click();
    }

});



function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}



function SignUp() {
    let s = {
        id: parseInt($("#id").val()),
        name: $("#usernameCreate").val(),
        email: $("#emailCreate").val(),
        password: $("#passwordCreate").val()
    };

    if (!validateEmail(s.email)) {
        alert("invalid Email Address format");
        return;
    }

    if (isNaN(s.id) == true || isNaN(s.name) == false)
    {
        alert("Id must be numberable and name Not !");
        return
    }
 

    
    $("#Error").empty();

    $.ajax({
        
        url: "/api/Users/",
        contentType: "application/json; charset=utf-8",
        type: "POST",
        data: JSON.stringify(s),
        success: function (data) {
            console.log(data);
            localStorage.setItem("email", $("#emailCreate").val());
            localStorage.setItem("password", $("#passwordCreate").val());
            localStorage.setItem("isAdmin", "False");
            window.location.replace("/Client/UserPage.html");
        },
        error: function (data) {
            alert(data.responseText)
        }

    });

    console.log("Finish Post");
}

function inErrorCase() {
    let $p = $("<p>").html("The Email or password is not correct.");
    $("#Error").append($p);
}
$(document).ajaxSend(function (event, jqXHR, settings) {

    $("#loader").css("display", "block");
});

$(document).ajaxComplete(function (event, jqXHR, settings) {
    $("#loader").fadeOut();
});