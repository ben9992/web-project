﻿using System;
using System.Web;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ClassLibrary1;


namespace WebApplication2.Controllers
{
    [RoutePrefix("api/Products")]
    public class ProductsController : ApiController
    {
        public dynamic Get()
        {
            CafeEntities db = new CafeEntities();
            return db.Products.Select(x => new
            {
                Name = x.name,
                Amount = x.amount,
                Catalog_number = x.catalog_number,
                Cost = x.cost,
                isAvailable = x.isAvailable,
                Category = x.Category.category_Name,
                Price = x.price,
                img_url = x.img_url
            }).Where(r => r.isAvailable == true).ToList();

        }
        [Route("All")]
        public dynamic GetAll()
        {
            CafeEntities db = new CafeEntities();
            return db.Products.Select(x => new
            {
                Name = x.name,
                Amount = x.amount,
                Catalog_number = x.catalog_number,
                Cost = x.cost,
                isAvailable = x.isAvailable,
                Category = x.Category.category_Name,
                Price = x.price,
                img_url = x.img_url
            }).ToList();

        }

        [Route("{email}/{password}")]
        // POST api/<controller>
        public HttpResponseMessage Post([FromBody] System.Collections.Generic.List<Purchase> p, string email, string password)
        {
            
            CafeEntities db = new CafeEntities();
            foreach (Purchase purchase in p)
            {
                Purchase p1 = new Purchase();
                TimeZoneInfo timeZoneInfo;
                DateTime dateTime;
                //Set the time zone information to US Mountain Standard Time 
                timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById("Israel Standard Time");
                //Get date and time in US Mountain Standard Time 
                dateTime = TimeZoneInfo.ConvertTime(DateTime.Now, timeZoneInfo);
                p1.purchase_date = dateTime;
                string name = purchase.number.ToString().Replace("\t", "");
                Product pr = db.Products.Where(x => x.name == name).SingleOrDefault();
                Client cr = db.Clients.Where(x => x.email == email && x.password == password).SingleOrDefault();

                if (pr != null && cr != null)
                {
                    p1.number = pr.catalog_number;
                    p1.amount = purchase.amount;
                    p1.student_id = cr.id;
                    db.Purchases.Add(p1);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound, "Not Found");
                }
            }
            db.SaveChanges();
            return Request.CreateResponse(HttpStatusCode.OK);
        }



        [Route("{id}")]
        public void Put(int id, [FromBody]string name)
        {
            CafeEntities db = new CafeEntities();
            string newName = name.Replace("\t", "");
            Product s = db.Products.SingleOrDefault(x => x.name == newName);
            
            if (s != null)
            {
                int newAmount = (int)s.amount - id;
                if (newAmount == 0)
                {
                    s.amount = 0;
                    s.isAvailable = false;
                }
                else
                    s.amount = newAmount;
                db.SaveChanges();
            }
        }

        [Route("{Catname}")]
        public HttpResponseMessage Post([FromBody]Product S, string Catname)
        {
            try
            {
                CafeEntities db = new CafeEntities();
                Product pro = new Product();
                pro.isAvailable = true;
                pro.name = S.name;
                pro.price = S.price;
                pro.cost = S.cost;
                pro.catalog_number = S.catalog_number;
                pro.amount = S.amount;
                pro.img_url = S.img_url;
                var temp = db.Categories.Where(x => x.category_Name == Catname).SingleOrDefault();
                pro.category_Id = temp.id;


                db.Products.Add(pro);
                db.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK, temp.category_Name);
            }
            catch { return Request.CreateResponse(HttpStatusCode.NotFound, "BadRequest - fail"); }
        }

        [HttpPut]
      
        public HttpResponseMessage Put()
        {
            try
            {
                HttpPostedFile f = HttpContext.Current.Request.Files["file"];
                string id = HttpContext.Current.Request.Params["id"];
                string ext = Path.GetExtension(f.FileName);


                CafeEntities db = new CafeEntities();
                Product pr = db.Products.SingleOrDefault(x => x.catalog_number == id.ToString());
                if (pr != null && f != null)
                {
                    pr.img_url = id.ToString() + f.FileName;
                    db.SaveChanges();

                    f.SaveAs(Path.Combine(HttpRuntime.AppDomainAppPath, @"Client\images\" + id.ToString() + f.FileName));
                    return Request.CreateResponse(HttpStatusCode.OK, f.FileName);
                }
                return Request.CreateResponse(HttpStatusCode.NotFound, "Upload Fail" );
            }
            catch(Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, "Upload Fail" + ex.Message);
            }
        }

    }
}