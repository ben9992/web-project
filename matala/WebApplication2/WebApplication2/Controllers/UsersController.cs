﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ClassLibrary1;

namespace WebApplication2.Controllers
{
    [RoutePrefix("api/Users")]
    public class UsersController : ApiController
    {

        [Route("{email}/{password}")]

        public HttpResponseMessage GetLogin(string email, string password)
        {
            CafeEntities db = new CafeEntities();
            Client s = db.Clients.SingleOrDefault(x => x.email == email && x.password == password);


            if (s != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, s.isAdmin);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, "Not Found");
            }

        }
        [Route("{id}/{name}/{email}")]
        [HttpPost]
        public HttpResponseMessage Post([FromUri]int id, [FromUri] string name, [FromUri]string email)
        {
            CafeEntities db = new CafeEntities();
            Client s = db.Clients.SingleOrDefault(x => x.email == email && x.id == id && x.name == name);

            if (s != null)
            {
                if (s.isAdmin == true)
                    return Request.CreateResponse(HttpStatusCode.NotFound, "Is already admin");
                s.isAdmin = true;
                db.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK, s.isAdmin);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, "Not Found");
            }
        }

        // POST api/<controller>
        [HttpGet]
        public dynamic GetUsers()
        {
            CafeEntities db = new CafeEntities();
            return db.Clients.Select(x => new
            {
                name = x.name,
                isAdmin = x.isAdmin,
                email = x.email,
                password = x.password,
                id = x.id
            }).ToList();
        }






        [HttpPost]
        public HttpResponseMessage Post(Client S)
        {
            try
            {
                CafeEntities db = new CafeEntities();

                S.isAdmin = false;

                Client R = db.Clients.Where(x => x.email == S.email).SingleOrDefault();

                if (R != null)
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound, S.email + " Is Found in Our List, Please contact your admin");
                }
                R = db.Clients.Where(x => x.id == S.id).SingleOrDefault();

                if (R != null)
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound, S.id + " Is Found in Our List, Please contact your admin");
                }


                db.Clients.Add(S);
                db.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK, "Created!");
            }

            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, "Exception at: " + ex.Message);
            }

        }
    }
}