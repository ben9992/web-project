using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ClassLibrary1;


namespace WebApplication2.Controllers
{
    [RoutePrefix("api/Purchases")]
    public class PurchasesController : ApiController
    {
           [Route("{email}/{password}")]//email,password to -> id
        [HttpGet]
        public dynamic Get(string password, string email)
        {

            CafeEntities db = new CafeEntities();

            Client client = db.Clients.Where(x => x.password == password && x.email == email).SingleOrDefault();

            return db.Purchases.Where(y=>y.Client.id == client.id).Select(x => new
            {
                number = x.number,
                amount = x.amount,
                purchase_date = x.purchase_date
            }).ToList();
            


        }

        // POST api/<controller>
        [Route("{email}/{password}")]
        [HttpPost]
        public HttpResponseMessage DeletePost([FromUri]string password, [FromUri] string email)
        {
            CafeEntities db = new CafeEntities();

            Client cr = db.Clients.Where(x => x.email == email && x.password == password).SingleOrDefault();
            if (cr != null)
            {
                List<Purchase> pr = db.Purchases.Where(x => x.student_id == cr.id).ToList();
                foreach (Purchase item in pr)
                {
                    db.Purchases.Remove(item);
                }
                db.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, "Not Found");
            }

        }

       
        
    }
}