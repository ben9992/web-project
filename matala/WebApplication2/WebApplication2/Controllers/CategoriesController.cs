﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ClassLibrary1;

namespace WebApplication2.Controllers
{
    [RoutePrefix("api/Categories")]
    public class CategoriesController : ApiController
    {
        [HttpGet]
        public dynamic GetCategories()
        {
            CafeEntities db = new CafeEntities();
            return db.Categories.Select(x => new
            {
                category_Name = x.category_Name
            }).ToList();
        
        }
        

        // POST api/<controller>

        
    }
}