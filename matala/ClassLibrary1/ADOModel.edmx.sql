
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 09/28/2018 23:41:06
-- Generated from EDMX file: C:\Users\user1\Documents\Visual Studio 2017\Projects\GcpProject1\ClassLibrary1\ADOModel.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [cafetiria];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK__Purchase__number__1A14E395]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Purchase] DROP CONSTRAINT [FK__Purchase__number__1A14E395];
GO
IF OBJECT_ID(N'[dbo].[FK__Purchase__studen__1920BF5C]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Purchase] DROP CONSTRAINT [FK__Purchase__studen__1920BF5C];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Categories]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Categories];
GO
IF OBJECT_ID(N'[dbo].[Clients]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Clients];
GO
IF OBJECT_ID(N'[dbo].[Products]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Products];
GO
IF OBJECT_ID(N'[dbo].[Purchase]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Purchase];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Products'
CREATE TABLE [dbo].[Products] (
    [name] nvarchar(30)  NULL,
    [price] float  NULL,
    [cost] float  NULL,
    [catalog_number] char(9)  NOT NULL,
    [amount] int  NULL,
    [isAvailable] bit  NULL,
    [category_Id] char(9)  NULL
);
GO

-- Creating table 'Purchases'
CREATE TABLE [dbo].[Purchases] (
    [student_id] int  NULL,
    [number] char(9)  NULL,
    [purchase_date] datetime  NULL,
    [amount] int  NULL,
    [purchase_id] int IDENTITY(1,1) NOT NULL
);
GO

-- Creating table 'Clients'
CREATE TABLE [dbo].[Clients] (
    [id] int  NOT NULL,
    [name] nvarchar(30)  NOT NULL,
    [is_active] bit  NULL,
    [avg_grade] float  NULL,
    [password] nvarchar(max)  NULL,
    [email] nvarchar(50)  NULL,
    [isUserLogin] bit  NOT NULL
);
GO

-- Creating table 'Categories'
CREATE TABLE [dbo].[Categories] (
    [category_Name] varchar(50)  NULL,
    [id] char(9)  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [catalog_number] in table 'Products'
ALTER TABLE [dbo].[Products]
ADD CONSTRAINT [PK_Products]
    PRIMARY KEY CLUSTERED ([catalog_number] ASC);
GO

-- Creating primary key on [purchase_id] in table 'Purchases'
ALTER TABLE [dbo].[Purchases]
ADD CONSTRAINT [PK_Purchases]
    PRIMARY KEY CLUSTERED ([purchase_id] ASC);
GO

-- Creating primary key on [id] in table 'Clients'
ALTER TABLE [dbo].[Clients]
ADD CONSTRAINT [PK_Clients]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'Categories'
ALTER TABLE [dbo].[Categories]
ADD CONSTRAINT [PK_Categories]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [student_id] in table 'Purchases'
ALTER TABLE [dbo].[Purchases]
ADD CONSTRAINT [FK__Purchase__studen__1920BF5C1]
    FOREIGN KEY ([student_id])
    REFERENCES [dbo].[Clients]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK__Purchase__studen__1920BF5C1'
CREATE INDEX [IX_FK__Purchase__studen__1920BF5C1]
ON [dbo].[Purchases]
    ([student_id]);
GO

-- Creating foreign key on [number] in table 'Purchases'
ALTER TABLE [dbo].[Purchases]
ADD CONSTRAINT [FK__Purchase__number__1A14E3951]
    FOREIGN KEY ([number])
    REFERENCES [dbo].[Products]
        ([catalog_number])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK__Purchase__number__1A14E3951'
CREATE INDEX [IX_FK__Purchase__number__1A14E3951]
ON [dbo].[Purchases]
    ([number]);
GO

-- Creating foreign key on [category_Id] in table 'Products'
ALTER TABLE [dbo].[Products]
ADD CONSTRAINT [FK_ProductCategory]
    FOREIGN KEY ([category_Id])
    REFERENCES [dbo].[Categories]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ProductCategory'
CREATE INDEX [IX_FK_ProductCategory]
ON [dbo].[Products]
    ([category_Id]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------