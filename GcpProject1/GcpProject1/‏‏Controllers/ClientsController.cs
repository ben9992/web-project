﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ClassLibrary1;

namespace WebApplication2.Controllers
{
    [RoutePrefix("api/Clients")]
    public class ClientsController : ApiController
    {
    
        // GET api/<controller>/5
    
        
        [Route("{email}/{password}")]
        [HttpGet]
        public HttpResponseMessage GetLogin(string email, string password)
        {
            cafetiriaEntities db = new cafetiriaEntities();
            Client s = db.Clients.SingleOrDefault(x => x.email == email && x.password == password);

            if (s != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK,"OK");
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NotFound,"Not Found");
            }
           
        }
      
        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}