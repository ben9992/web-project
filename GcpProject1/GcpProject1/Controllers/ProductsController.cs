﻿using System;

using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ClassLibrary1;


namespace WebApplication2.Controllers
{
    [RoutePrefix("api/Products")]
    public class ProductsController : ApiController
    {

        // GET api/<controller>/5
        public dynamic Get()
        {
            CafeEntities db = new CafeEntities();
            return db.Products.Select(x => new
            {
                Name = x.name,
                Amount = x.amount,
                Catalog_number = x.catalog_number,
                Cost = x.cost,
                isAvailable = x.isAvailable,
                Category = x.Category.category_Name,
                Price = x.price,
                img_url = x.img_url
            }).Where(r => r.isAvailable == true).ToList();

        }
        [Route("{id}")]
        [HttpGet]
        public HttpResponseMessage GetProductName(int id)
        {
            CafeEntities db = new CafeEntities();
            Product s = db.Products.Where(x => x.catalog_number == id.ToString()).SingleOrDefault();

            if (s != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, s.name);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, "Not Found");
            }

        }



        [Route("{name}{password}{email}")]
        // POST api/<controller>
        public void Post([FromBody]Purchase p, string name, string password, string email)
        {
            CafeEntities db = new CafeEntities();
            Purchase p1 = new Purchase();
            TimeZoneInfo timeZoneInfo;
            DateTime dateTime;
            //Set the time zone information to US Mountain Standard Time 
            timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById("Israel Standard Time");
            //Get date and time in US Mountain Standard Time 
            dateTime = TimeZoneInfo.ConvertTime(DateTime.Now, timeZoneInfo);
            p1.purchase_date = dateTime;
            Product pr = db.Products.Where(x => x.name == name).SingleOrDefault();
            Client cr = db.Clients.Where(x => x.email == email && x.password == password).SingleOrDefault();
            p1.number = pr.catalog_number;
            p1.amount = p.amount;
            p1.student_id = cr.id;
            db.Purchases.Add(p1);
            db.SaveChanges();
        }



        [Route("{id}")]
        public void Put(int id, [FromBody]string name)
        {
            CafeEntities db = new CafeEntities();
            Product s = db.Products.SingleOrDefault(x => x.name == name);
            
            if (s != null)
            {
                int newAmount = (int)s.amount - id;
                if (newAmount == 0)
                {
                    s.amount = 0;
                    s.isAvailable = false;
                }
                else
                    s.amount = newAmount;
                db.SaveChanges();
            }
        }
        
        
            
        
        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}