﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ClassLibrary1;

namespace WebApplication2.Controllers
{
    [RoutePrefix("api/Categories")]
    public class CategoriesController : ApiController
    {
        [Route("{id}")]
        [HttpGet]
        public List<Category> GetCategories(int id)
        {
            CafeEntities db = new CafeEntities();
            List<Category> s = db.Categories.ToList();
            return s;
        }

        // POST api/<controller>
        [Route("{Catname}")]
        public void Post([FromBody]Product S,string Catname)
            {
            CafeEntities db = new CafeEntities();
                Product pro = new Product();
            pro.isAvailable = true;
                pro.name = S.name;
                pro.price = S.price;
                pro.cost = S.cost;
                pro.catalog_number = S.catalog_number;
                pro.amount = S.amount;
                
                var temp = db.Categories.Where(x => x.category_Name == Catname).SingleOrDefault();
                pro.category_Id = temp.id;


                db.Products.Add(pro);
                db.SaveChanges();
            }
        

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}