﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ClassLibrary1;

namespace WebApplication2.Controllers
{
    [RoutePrefix("api/Clients")]
    public class ClientsController : ApiController
    {
        [Route("{id}")]
        [HttpGet]
        public HttpResponseMessage GetName(int id)
        {
            CafeEntities db = new CafeEntities();
            Client s = db.Clients.Where(x => x.id == id).SingleOrDefault();

            if (s != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, s.name);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, "Not Found");
            }

        }

  

        [Route("{email}/{password}")]
        [HttpGet]
        public HttpResponseMessage GetLogin(string email, string password)
        {
            CafeEntities db = new CafeEntities();
            Client s = db.Clients.SingleOrDefault(x => x.email == email && x.password == password);


            if (s != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK,s.isAdmin);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NotFound,"Not Found");
            }
           
        }
        [Route("{id}/{name}/{email}")]
        // DELETE api/<controller>/5
        public HttpResponseMessage Post(int id,string name,string email)
        {
            CafeEntities db = new CafeEntities();
            Client s = db.Clients.SingleOrDefault(x => x.email == email && x.id == id && x.name==name);
           
            if (s != null)
            {
                if (s.isAdmin == true)
                    return Request.CreateResponse(HttpStatusCode.NotFound, "Is already admin");
                s.isAdmin = true;
                db.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK, s.isAdmin);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, "Not Found");
            }
        }
    }
}