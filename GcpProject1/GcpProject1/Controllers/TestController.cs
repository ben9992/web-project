﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ClassLibrary1;
namespace WebApplication2.Controllers
{
    public class TestController : ApiController
    {


        [Route("api/Test/{password}{email}")]
        // POST api/<controller>
        public HttpResponseMessage Post( string password, string email)
        {
            CafeEntities db = new CafeEntities();

            Client cr = db.Clients.Where(x => x.email == email && x.password == password).SingleOrDefault();
            if (cr != null)
            {
                List<Purchase> pr = db.Purchases.Where(x => x.student_id == cr.id).ToList();
                foreach (Purchase item in pr)
                {
                    db.Purchases.Remove(item);
                }
                db.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, "Not Found");
            }
            
        }
        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}