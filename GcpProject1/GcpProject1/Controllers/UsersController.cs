﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ClassLibrary1;

namespace WebApplication2.Controllers
{
    public class UsersController : ApiController
    {

        // POST api/<controller>
        [Route("Users/{id}")]
        [HttpGet]
        public dynamic GetUsers(int id)
        {
            CafeEntities db = new CafeEntities();
            return db.Clients.Select(x => new
            {
                name = x.name,
                isAdmin = x.isAdmin,
                email = x.email,
                password = x.password,
                id = x.id
            }).ToList();

        }

        [HttpPost]
        [Route("/api/Users/HttpPost")]
        public HttpResponseMessage Post(Client S)
        {
            CafeEntities db = new CafeEntities();
            S.isAdmin = false;

            Client R = db.Clients.Where(x => x.email == S.email).SingleOrDefault();

            if (R != null)
            {
                return Request.CreateResponse(HttpStatusCode.Conflict, "Is Found in Our List x " + R.email + " "+ S.email);
            }

            
            db.Clients.Add(S);
            db.SaveChanges();
            return Request.CreateResponse(HttpStatusCode.OK,"Created!");
        }
       
    }
}