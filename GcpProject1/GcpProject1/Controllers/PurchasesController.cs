using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ClassLibrary1;
using System.Web;
using System.IO;

namespace WebApplication2.Controllers
{
    public class PurchasesController : ApiController
    {
        [Route("api/Purchases/{password}/{email}")]//email,password to -> id
        public List<Purchase> Get(string password, string email)
        {

            CafeEntities db = new CafeEntities();

            Client client = db.Clients.Where(x => x.password == password && x.email == email).SingleOrDefault();
            

            List<Purchase> PList = db.Purchases.Where(x => x.Client.id == client.id).ToList();
            return PList;


        }

        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        [Route("api/Purchases/{id}")]
        public void Put(int id)
        {
            HttpPostedFile f = HttpContext.Current.Request.Files["file"];
            string ext = Path.GetExtension(f.FileName);
           
           
            

            
            CafeEntities db = new CafeEntities();
            Product pr = db.Products.SingleOrDefault(x => x.catalog_number == id.ToString());
            if (pr != null)
            {
                pr.img_url =  id.ToString() + f.FileName;
                db.SaveChangesAsync();
                
                f.SaveAs(Path.Combine(HttpRuntime.AppDomainAppPath, @"Client\images\" + id.ToString() + f.FileName));
            }
        }
        
        


        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}